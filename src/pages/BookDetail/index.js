//import Redux
import {connect} from 'react-redux'
//import Action
import {
    getBookDetail, 
    postBookFavorite, 
    getFavoriteStatus
} from 'bookappredux2/src/redux/actions/BookDetailAction'
//import Component
import {BookDetail} from 'bookappredux2/src/pages/BookDetail/component'

const mapStateToProps = (state) => ({
    dataBookDetail: state.BookDetailReducer
})  
const mapDispatchToProps = {
    postBookFavorite,
    getBookDetail,
    getFavoriteStatus,
}
  
export default connect(mapStateToProps, mapDispatchToProps) (BookDetail)
