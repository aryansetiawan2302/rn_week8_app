import {requestServer} from 'bookappredux2/src/api/api'

export const GET_BOOK_TITLE_SEARCH_START = 'GET_BOOK_TITLE_SEARCH_START';
export const GET_BOOK_TITLE_SEARCH_SUCCESS = 'GET_BOOK_TITLE_SEARCH_SUCCESS';
export const GET_BOOK_TITLE_SEARCH_FAILED = 'GET_BOOK_TITLE_SEARCH_FAILED';


capitalizeTheFirstLetterOfEachWord = (words) => {
    const separateWord = words.toLowerCase().split(' ');
    for (let i = 0; i < separateWord.length; i++) {
       separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
       separateWord[i].substring(1);
    }
    return separateWord.join(' ');
 }

export const getBookTitleSearch = (bookTitle) => async dispatch => {
   
    try{
        dispatch({
            type: GET_BOOK_TITLE_SEARCH_START,
        })

        // console.log(bookTitle, 'sebelum kapitalisasi')
        const capitalizedBookTitle = capitalizeTheFirstLetterOfEachWord(bookTitle)
        // console.log(capitalizedBookTitle, 'sesudah kapitalisasi')
        
        const hasilFetchingTotal = await requestServer('get', `/api/v1/books?title=${bookTitle}|${capitalizedBookTitle}`)
        const totalResults = hasilFetchingTotal.data.totalResults

        const hasilFetching = await requestServer('get', `/api/v1/books?title=${bookTitle}|${capitalizedBookTitle}&limit=${totalResults}`)

        if (hasilFetching.status === 200) {

            if(bookTitle !== ''){
                dispatch({
                    type: GET_BOOK_TITLE_SEARCH_SUCCESS,
                    dataSearchBookList: hasilFetching.data.results, 
                })
                // console.log('end of action')    
            } else {
                dispatch({
                    type: GET_BOOK_TITLE_SEARCH_SUCCESS,
                    dataSearchBookList: [], 
                })
                // console.log('end of action')    
            }
        }
    } catch(error) {
        dispatch({
            type: GET_BOOK_TITLE_SEARCH_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error')
    }
}
