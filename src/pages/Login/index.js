//import Redux
import {connect} from 'react-redux'
//import Action
import {submitDataLogin} from 'bookappredux2/src/redux/actions/LoginAction'
//import Component
import {Login} from 'bookappredux2/src/pages/Login/component'

const mapStateToProps = (state) => ({
    dataLogin: state.LoginReducer
})
  
const mapDispatchToProps = {
    submitDataLogin
}
  
export default connect(mapStateToProps, mapDispatchToProps) (Login)
