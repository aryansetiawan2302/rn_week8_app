//import Redux
import {connect} from 'react-redux'
//import Action
import {submitDataReg} from 'bookappredux2/src/redux/actions/RegisterAction'
//import Component
import {Register} from 'bookappredux2/src/pages/Register/component'

const mapStateToProps = (state) => ({
    dataReg: state.RegisterReducer
})
  
const mapDispatchToProps = {
    submitDataReg
}
  
export default connect(mapStateToProps, mapDispatchToProps) (Register)
