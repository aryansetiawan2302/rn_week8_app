import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import RegisterReducer from 'bookappredux2/src/redux/reducers/RegisterReducer'
import LoginReducer from 'bookappredux2/src/redux/reducers/LoginReducer'
import HomeReducer from 'bookappredux2/src/redux/reducers/HomeReducer'
import BookDetailReducer from 'bookappredux2/src/redux/reducers/BookDetailReducer'
import SearchReducer from 'bookappredux2/src/redux/reducers/SearchReducer'
import FavoriteReducer from 'bookappredux2/src/redux/reducers/FavoriteReducer'

const allReducers = combineReducers({
    RegisterReducer, 
    LoginReducer,
    HomeReducer,
    BookDetailReducer,
    SearchReducer,
    FavoriteReducer,
})

export const bookStore = createStore(allReducers, applyMiddleware(thunk))

// import { createStore, combineReducers, applyMiddleware } from 'redux';
// import thunk from 'redux-thunk';
// import RegisterReducer from './reducers/RegisterReducer'

// const allReducers = combineReducers({
//     RegisterReducer, 
// })

// export const bookStore = createStore(allReducers, applyMiddleware(thunk))


// // redux-persist
// import { persistStore, persistReducer } from 'redux-persist'
// import AsyncStorage from '@react-native-async-storage/async-storage';

// const persistConfig = {
//     key: 'root',
//     storage: AsyncStorage,
// }

// const allReducers = combineReducers({
//     RegisterReducer, 
//     LoginReducer,
//     HomeReducer,
//     BookDetailReducer,
//     SearchReducer,
//     FavoriteReducer,
// })

// const permanentReducer = persistReducer(persistConfig, allReducers)

// // export const bookStore = createStore(allReducers, applyMiddleware(thunk))
// export const bookStore = createStore(permanentReducer, applyMiddleware(thunk))
// export const permanentStore = persistStore(bookStore)
