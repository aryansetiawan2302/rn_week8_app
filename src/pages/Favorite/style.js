import { StyleSheet, Dimensions } from "react-native";
import { 
    WARNA_BG_SCROLL,
    WARNA_FAVORIT1,
    WARNA_FAVORIT2,
    WARNA_FAVORIT3,
    WARNA_FAVORIT4,
    WARNA_SHADOW,
} from 'bookappredux2/src/utils/constant'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({

    background: {
        position:'absolute',
        left:0, 
        right:0, 
        top:0,
        bottom:0
    },
    scrollBackground: {
        backgroundColor: WARNA_BG_SCROLL,
    },
    loading: {
        width: windowWidth,
        height: windowHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },
    sectionText: {
        marginVertical: 10,
        marginLeft: 10,
        fontSize: 22,
        fontFamily: 'LilitaOne-Regular',
        textAlign: 'center',
    },
    favBookContainer: (index) => ({
        backgroundColor: (
            index % 4 === 3 ? WARNA_FAVORIT4 : 
            index % 4 === 2 ? WARNA_FAVORIT3 : 
            index % 4 === 1 ? WARNA_FAVORIT2 : 
            WARNA_FAVORIT1
        ),
        paddingVertical: 5,
        paddingHorizontal: 20,
        marginHorizontal: 30,
        borderRadius: 10,
        shadowColor: WARNA_SHADOW,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        marginTop: 10,
        flexDirection: 'row',
    }),
    rightFavContainer: {
        flex: 1,
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginLeft: 10
    },

    favBookImage: {
        width: 500*0.15,
        height: 750*0.15,
        borderRadius: 15,
    },
    titleText: {
        fontSize: 15,
        fontFamily: 'JosefinSans-Bold'
    },
    authorText: {
        fontSize: 12,
        fontFamily: 'JosefinSans-Italic'
    },
    
});
