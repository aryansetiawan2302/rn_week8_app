import AsyncStorage from '@react-native-async-storage/async-storage';
import {requestServer} from 'bookappredux2/src/api/api'

export const SUBMIT_LOGIN_START = 'SUBMIT_LOGIN_START';
export const SUBMIT_LOGIN_SUCCESS = 'SUBMIT_LOGIN_SUCCESS';
export const SUBMIT_LOGIN_FAILED = 'SUBMIT_LOGIN_FAILED';
export const UPDATE_STATUS_LOGIN = 'UPDATE_STATUS_LOGIN';

export const submitDataLogin = requestBodyLogin => async dispatch => {

    console.log('exe submitDataLogin')
    try{
        dispatch({
            type: SUBMIT_LOGIN_START,
        })
        const response = await requestServer('post', `/api/v1/auth/login`, requestBodyLogin)

        if (response.status === 200) {


            dispatch({
                type: SUBMIT_LOGIN_SUCCESS,
                name: response.data.user.name,
            })

            // console.log('start asyncStorage');
            await AsyncStorage.setItem(
                'token-user',
                response.data.tokens.access.token,
            );

            console.log(response.data.user.name, 'ini nama user')
            console.log(response.data.tokens.access.token, 'ini data token')
        }

    }catch(error){
        dispatch({
            type: SUBMIT_LOGIN_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error')
    }
}

export const updateStatusLogin = () => {
    return {
        type: UPDATE_STATUS_LOGIN,
    }
}
