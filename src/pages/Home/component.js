import React, { Component } from 'react'
import { 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity, 
    Image, 
    ActivityIndicator,
    ImageBackground
} from 'react-native'

import { SplashBackground } from 'bookappredux2/src/assets'
import {styles} from 'bookappredux2/src/pages/Home/style'

export class Home extends Component {

    async componentDidMount(){
        await this.props.updateStatusLogin()
        this.props.getBookList()
    }

    render() {
        // console.log(this.props, 'ini props Home')
        return (
            <>
            <ImageBackground source={SplashBackground} style={styles.background}></ImageBackground>            
            <ScrollView style={styles.scrollBackground}>
            
            {this.props.dataBook.isLoading ? (
                <View style={styles.loading}>
                    <ActivityIndicator size='large' color='red'/>
                </View>
            ): 
            
            <View>

                <Text style={styles.greetingText}> Hello {this.props.dataLogin.name}!</Text>
                <Text style={styles.sectionText}> Recommended </Text>

                <ScrollView horizontal={true}>

                    {console.log('checkpoint')}
                    <View style={styles.recBookListContainer}>
                        {this.props.dataBook.listBook.map(book => (
                            <TouchableOpacity
                                key={book.id}
                                onPress={()=>{
                                    this.props.navigation.navigate('BookDetail', {BookId: book.id})
                                }}                                        
                            >
                                <View style={styles.recBookContainer}>
                                    <Image 
                                        style={styles.recBookImage}
                                        source={{
                                            uri: book.cover_image,
                                        }}
                                    />
                                    <Text style={styles.recBookTitle}>{book.title}</Text>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>

                </ScrollView>

                <Text style={styles.sectionText}> Popular Books </Text>

                <View style={styles.popBookListContainer}>
                    {this.props.dataBook.listBook
                        .sort((a, b) => b.average_rating - a.average_rating)
                        .map(book => (
                        <TouchableOpacity
                            key={book.id}
                            onPress={()=>{
                                this.props.navigation.navigate('BookDetail', {BookId: book.id})
                            }}                                        
                            style={styles.popBookContainer}
                        >
                            <Image 
                                style={styles.popBookImage}
                                source={{
                                    uri: book.cover_image,
                                }}
                            />
                            <Text style={styles.popBookTitle}>{book.title}</Text>

                        </TouchableOpacity>
                    ))}
                </View>
            </View>

            }
            </ScrollView>
            </>
        )
    }
}
