import { StyleSheet, Dimensions } from "react-native";
import { 
    WARNA_UTAMA,
    WARNA_SHADOW,
    WARNA_BG_CONTAINER,
    WARNA_TEXT_BUTTON,
} from 'bookappredux2/src/utils/constant'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({

    loading: {
        width: windowWidth,
        height: windowHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },

    // TopIcon
    IconContainer: {
        flexDirection: 'row',
        margin: 10,
    },
    topIconBack: { 
        flex: 1,
        padding: 3, 
        marginHorizontal: 10,
        backgroundColor: WARNA_UTAMA, 
        borderRadius: 20, 
        alignItems: 'center'
    },
    topIconFav: { 
        flex: 3,
        padding: 3, 
        marginHorizontal: 10,
        backgroundColor: WARNA_UTAMA, 
        borderRadius: 20,
        opacity: 0.75,
        alignItems: 'center'
    },
    topIconShare: { 
        flex: 1,
        padding: 3, 
        marginHorizontal: 10,
        backgroundColor: WARNA_UTAMA, 
        borderRadius: 20, 
        opacity: 0.75,
        alignItems: 'center'
    },

    // Top container
    topContainer: {
        backgroundColor: WARNA_BG_CONTAINER,
        padding: 17,
        marginHorizontal: 30,
        borderRadius: 10,
        shadowColor: WARNA_SHADOW,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        marginTop: 10,
        flexDirection: 'row',
    },
    smallPoster: {
        width: 500*0.2,
        height: 750*0.2,
        marginRight: 15,
        borderRadius: 10,
    },
    titleText: {
        fontSize: 15,
        fontFamily: 'JosefinSans-Bold'
    },
    authorText: {
        fontSize: 12,
        fontFamily: 'JosefinSans-Italic'
    },
    ratingView: {
        flex: 1,
    },
    rightTopContainer: {
        flex: 1,
        flexWrap: 'wrap',
        alignItems: 'flex-start'
    },

    // Mid container
    midContainer: {
        backgroundColor: WARNA_BG_CONTAINER,
        padding: 17,
        marginHorizontal: 30,
        borderRadius: 10,
        shadowColor: WARNA_SHADOW,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    midContainerItem: {
        alignItems: 'center'
    },
    midContainerButton: {
        flexDirection: 'row',
        backgroundColor: WARNA_UTAMA,
        borderRadius: 10,
        paddingHorizontal: 10,
        alignItems:'center'
    },
    midContainerText: {
        fontFamily: 'JosefinSans-Bold'
    },
    midContainerNumber: {
        fontFamily: 'JosefinSans-Regular'
    },
    midContainerButtonText: {
        color: WARNA_TEXT_BUTTON,
        fontFamily: 'JosefinSans-Bold'
    },
    
    // Bottom Container
    bottomContainer: {
        padding: 17,
        marginHorizontal: 30,
        borderRadius: 10,
        marginTop: 10,
    },
    overviewText: {
        fontSize: 25,
        marginBottom: 10,
        fontFamily: 'GoudyBookletter1911-Regular'        
    },
    contentText: {
        fontSize: 15,
        fontFamily: 'GoudyBookletter1911-Regular'
    },

})

