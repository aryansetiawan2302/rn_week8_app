import Home from 'bookappredux2/src/pages/Home'
import Search from 'bookappredux2/src/pages/Search'
import Favorite from 'bookappredux2/src/pages/Favorite'

import Splash from 'bookappredux2/src/pages/Splash'
import Login from 'bookappredux2/src/pages/Login'
import Register from 'bookappredux2/src/pages/Register'
import SuccessReg from 'bookappredux2/src/pages/SuccessReg'
import BookDetail from 'bookappredux2/src/pages/BookDetail'

export {
    Home, 
    Search, 
    Favorite, 
    Splash, 
    Login, 
    Register, 
    SuccessReg, 
    BookDetail
}