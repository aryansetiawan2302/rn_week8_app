import {
    GET_BOOK_TITLE_SEARCH_START,
    GET_BOOK_TITLE_SEARCH_SUCCESS,
    GET_BOOK_TITLE_SEARCH_FAILED,
} from 'bookappredux2/src/redux/actions/SearchAction'

const initialState = {
    listBookSearch: [],
    isLoading: false,
    errorMessage: null,
}

export default (state = initialState, action) => {
    // console.log(state, 'ini state home yang terpanggil')
    // console.log(action.type, 'ini action type di Reducer')
    // console.log(action.dataSearchBookList, 'ini dataSearchBookList di Reducer')

    switch (action.type) {
        case GET_BOOK_TITLE_SEARCH_SUCCESS:
            return { ...state, listBookSearch: action.dataSearchBookList}
        case GET_BOOK_TITLE_SEARCH_FAILED:
            return { ...state, errorMessage: action.errorMessage}

    default:
        return state
    }
}
