import {
    SUBMIT_LOGIN_START,
    SUBMIT_LOGIN_SUCCESS,
    SUBMIT_LOGIN_FAILED,
    UPDATE_STATUS_LOGIN,
} from 'bookappredux2/src/redux/actions/LoginAction'

const initialState = {
    name: null,
    isLoginSuccess: false,
    isLoading: false,
    errorMessage: null,
}

export default (state = initialState, action) => {
    // console.log(state, 'ini state login yang terpanggil')
    // console.log(action.name, 'ini action.name')

    switch (action.type) {
        case SUBMIT_LOGIN_SUCCESS:
            return { ...state, isLoginSuccess: true, errorMessage: null, name: action.name}
        case SUBMIT_LOGIN_FAILED:
            return { ...state, errorMessage: action.errorMessage}
        case UPDATE_STATUS_LOGIN:
            return { ...state, isLoginSuccess: false}
    
    default:
        return state
    }
}
