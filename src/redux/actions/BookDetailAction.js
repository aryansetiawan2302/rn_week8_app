// import axios from 'axios'
// import AsyncStorage from '@react-native-async-storage/async-storage';

// import {requestServer} from '../../api/api'
import {requestServer} from 'bookappredux2/src/api/api'

export const GET_BOOK_DETAIL_START = 'GET_BOOK_DETAIL_START';
export const GET_BOOK_DETAIL_SUCCESS = 'GET_BOOK_DETAIL_SUCCESS';
export const GET_BOOK_DETAIL_FAILED = 'GET_BOOK_DETAIL_FAILED';

export const POST_BOOK_FAVORITE_START = 'POST_BOOK_FAVORITE_START';
export const POST_BOOK_FAVORITE_SUCCESS = 'POST_BOOK_FAVORITE_SUCCESS';
export const POST_BOOK_FAVORITE_FAILED = 'POST_BOOK_FAVORITE_FAILED';

export const GET_FAVORITE_STATUS_START = 'GET_FAVORITE_STATUS_START';
export const GET_FAVORITE_STATUS_SUCCESS = 'GET_FAVORITE_STATUS_SUCCESS';
export const GET_FAVORITE_STATUS_FAILED = 'GET_FAVORITE_STATUS_FAILED';


export const getFavoriteStatus = (bookId) => async dispatch => {
   
    try{
        dispatch({
            type: GET_FAVORITE_STATUS_START,
        })

        // const tokenDariLocalStorage = await AsyncStorage.getItem('token-user');
        // console.log(tokenDariLocalStorage, 'ini token')

        // //cth long code
        // const hasilFetchingTotal = await axios.get(
        //     'http://code.aldipee.com/api/v1/books/my-favorite',
        //     { headers: {
        //         Authorization: `Bearer ${tokenDariLocalStorage}`
        //     }}
        // )

        const hasilFetchingTotal = await requestServer('get', '/api/v1/books/my-favorite')
        // console.log(hasilFetchingTotal, 'ini hasil fetching')
        
        const totalResults = hasilFetchingTotal.data.totalResults

        const hasilFetching = await requestServer('get', `/api/v1/books/my-favorite?limit=${totalResults}`)
        // console.log(hasilFetching.data, 'ini hasilFetching di favorite')

        if (hasilFetching.status === 200) {

            const isFavorite = hasilFetching.data.some(x => x._id == bookId)

            dispatch({
                type: GET_FAVORITE_STATUS_SUCCESS,
                dataIsFavorite: isFavorite, 
            })
            console.log(isFavorite, 'ini status favorite')
        }

    } catch(error) {
        dispatch({
            type: GET_FAVORITE_STATUS_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error')
    }
}

export const postBookFavorite = requestBodyFav => async dispatch => {
   
    try{
        dispatch({
            type: POST_BOOK_FAVORITE_START,
        })

        // const tokenDariLocalStorage = await AsyncStorage.getItem('token-user');

        // //cth long code
        // const response = await axios.post(
        //     'http://code.aldipee.com/api/v1/books/my-favorite', 
        //     requestBodyFav,
        //     { headers: {
        //         Authorization: `Bearer ${tokenDariLocalStorage}`
        //     }},
        // )
        const response = await requestServer('post', `/api/v1/books/my-favorite`, requestBodyFav)

        // console.log(response, 'ini response fav')
        if (response.status === 201){
            dispatch({
                type: POST_BOOK_FAVORITE_SUCCESS
            })
        }

    }catch(error){
        dispatch({
            type: POST_BOOK_FAVORITE_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error fav')
    }
}


export const getBookDetail = (bookId) => async dispatch => {
   
    try{
        dispatch({
            type: GET_BOOK_DETAIL_START,
        })

        const hasilFetching = await requestServer('get', `/api/v1/books/${bookId}`)

        if (hasilFetching.status === 200) {

            dispatch({
                type: GET_BOOK_DETAIL_SUCCESS,
                dataBookDetail: hasilFetching.data,
            })
            // console.log(hasilFetching.data, 'ini data.results')
            console.log('getBookDetail berhasil')
        }

    } catch(error) {
        dispatch({
            type: GET_BOOK_DETAIL_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error')
    }
}
