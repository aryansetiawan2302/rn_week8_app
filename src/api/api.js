import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { CONFIGURATION } from 'bookappredux2/src/config/url'

export async function requestServer(method, path, bodyData = null){
    const tokenDariLocalStorage = await AsyncStorage.getItem('token-user')
    // console.log(tokenDariLocalStorage, 'ini token di api')
    return axios({
        method: method,
        url: CONFIGURATION.ENDPOINT.concat(path),
        data: {
            ...bodyData
        },
        headers: tokenDariLocalStorage !== null ?
            {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            } :
        {}
    })
}