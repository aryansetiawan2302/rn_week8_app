import React, { Component } from 'react'
// import { Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import Router from './src/router';
import { Provider } from 'react-redux'
import { bookStore } from './src/redux/store';

export class App extends Component {
  render() {
    return (
      <Provider store={bookStore}>
        <NavigationContainer>
          <Router />
        </NavigationContainer>    
      </Provider>
    )
  }
}

export default App

// redux persist
// import { bookStore, permanentStore } from './src/redux/store';
// import { PersistGate } from 'redux-persist/integration/react'
// import { PersistGate } from 'redux-persist/lib/integration/react'

// export class App extends Component {
//   render() {
//     return (
//       <Provider store={bookStore}>
//         {/* <PersistGate loading={null} persistor={permanentStore}> */}
//           <NavigationContainer>
//             <Router />
//           </NavigationContainer>   
//         {/* </PersistGate>  */}
//       </Provider>
//     )
//   }
// }