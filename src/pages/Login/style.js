import { StyleSheet } from "react-native";
import { 
    WARNA_UTAMA, 
    WARNA_WARNING, 
    WARNA_DISABLE,
    WARNA_TEXT_FORM,
    WARNA_BG_FORM,
    WARNA_TEXT_BUTTON,
    WARNA_TEXT_WARNING,
} from 'bookappredux2/src/utils/constant'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 40,
    },
    welcomeText: {
        fontSize: 35,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    errorMessageView: {
        marginVertical: 20,
        backgroundColor: WARNA_WARNING,
        padding: 5,
        borderRadius: 5,
        width: 280,
    },
    errorMessageText: {
        fontSize: 15,
        textAlign: 'center',
        color: WARNA_TEXT_WARNING,
    },

    formTextInput: {
        padding: 5,
        backgroundColor: WARNA_BG_FORM,
        color: WARNA_TEXT_FORM,
        width: 250,
        borderRadius: 10,
        marginVertical: 5,
    },
    formValidationText: {
        fontSize: 10,
        marginTop: -5,
        color: WARNA_WARNING,
        fontWeight: 'bold',
        fontStyle: 'italic',
        marginLeft: 10,
    },

    loginButtonText: {
        fontSize: 15,
        color: WARNA_TEXT_BUTTON,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    loginButtonDisabled: {
        marginVertical: 30,
        backgroundColor: WARNA_DISABLE,
        paddingHorizontal: 30,
        paddingVertical: 10,
        borderRadius: 10,
        width: 250,
    },
    loginButton: {
        marginVertical: 30,
        backgroundColor: WARNA_UTAMA,
        paddingHorizontal: 30,
        paddingVertical: 10,
        borderRadius: 10,
        width: 250,
    },
    registerButtonText: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    registerButton: {
        paddingVertical: 5,
    },
    logo: {
        width: 230,
        height: 205,
        marginBottom: 30,
    },
})