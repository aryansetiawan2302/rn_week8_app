import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image } from 'react-native'
import {connect} from 'react-redux'
import {updateStatusReg} from 'bookappredux2/src/redux/actions/RegisterAction'
import { SuccessLogo } from 'bookappredux2/src/assets';
import {styles} from 'bookappredux2/src/pages/SuccessReg/style'

export class SuccessReg extends Component {

    constructor(){
        super()
    }

    async componentDidMount(){
        await this.props.updateStatusReg()
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.successText}>Registration Completed!</Text>
                <Image source={SuccessLogo} style={styles.logo}/>

                <Text style={styles.directionText}>We sent email verification to your email</Text>
                <TouchableOpacity
                    style={styles.backButton}
                    onPress={()=>{
                        this.props.navigation.navigate('Login')
                    }}                                                        
                >
                    <Text style={styles.buttonText}>Back to login</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    dataReg: state.RegisterReducer
})
  
const mapDispatchToProps = {
    updateStatusReg
}
  
export default connect(mapStateToProps, mapDispatchToProps) (SuccessReg)
