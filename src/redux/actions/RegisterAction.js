import {requestServer} from 'bookappredux2/src/api/api'

export const SUBMIT_REG_START = 'SUBMIT_REG_START';
export const SUBMIT_REG_SUCCESS = 'SUBMIT_REG_SUCCESS';
export const SUBMIT_REG_FAILED = 'SUBMIT_REG_FAILED';
export const UPDATE_STATUS_REG = 'UPDATE_STATUS_REG';

export const submitDataReg = requestBodyReg => async dispatch => {

    try{
        dispatch({
            type: SUBMIT_REG_START,
        })
        const response = await requestServer('post', `/api/v1/auth/register`, requestBodyReg)

        // console.log(response, 'ini response')
        if (response.status === 201){
            dispatch({
                type: SUBMIT_REG_SUCCESS
            })
        }

    }catch(error){
        dispatch({
            type: SUBMIT_REG_FAILED,
            errorMessage: error.response.data.message
        })
        // console.log(error, 'ini error')
        // console.log(error.response, 'ini response error')
        console.log(error.response.data.message, 'ini message error')
    }
}

export const updateStatusReg = () => {
    console.log('action update status')
    return {
        type: UPDATE_STATUS_REG,
        // isRegisterSuccess: false,
    }
}