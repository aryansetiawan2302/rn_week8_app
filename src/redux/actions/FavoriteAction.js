import {requestServer} from '../../api/api'

export const GET_BOOK_FAVORITE_START = 'GET_BOOK_FAVORITE_START';
export const GET_BOOK_FAVORITE_SUCCESS = 'GET_BOOK_FAVORITE_SUCCESS';
export const GET_BOOK_FAVORITE_FAILED = 'GET_BOOK_FAVORITE_FAILED';

export const getBookFavorite = () => async dispatch => {
   
    try{
        dispatch({
            type: GET_BOOK_FAVORITE_START,
        })

        const hasilFetchingTotal = await requestServer('get', '/api/v1/books/my-favorite')
        const totalResults = hasilFetchingTotal.data.totalResults
        const hasilFetching = await requestServer('get', `/api/v1/books/my-favorite?limit=${totalResults}`)
        // console.log(hasilFetching.data, 'ini hasilFetching di favorite')

        if (hasilFetching.status === 200) {

            dispatch({
                type: GET_BOOK_FAVORITE_SUCCESS,
                dataBookFavorite: hasilFetching.data, //masih terbatas 10 buku
            })
            // console.log(hasilFetching.data.results, 'ini data.results')
        }

    } catch(error) {
        dispatch({
            type: GET_BOOK_FAVORITE_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error')
    }
}