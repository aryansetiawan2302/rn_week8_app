
import {connect} from 'react-redux'

import {getBookFavorite} from 'bookappredux2/src/redux/actions/FavoriteAction'

import {Favorite} from 'bookappredux2/src/pages/Favorite/component'

const mapStateToProps = (state) => ({
    dataBookFavorite: state.FavoriteReducer,
    dataBookDetail: state.BookDetailReducer    
})
  const mapDispatchToProps = {
    getBookFavorite
}
  
export default connect(mapStateToProps, mapDispatchToProps) (Favorite)
