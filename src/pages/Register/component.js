import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity } from 'react-native'
import {styles} from 'bookappredux2/src/pages/Register/style'

export class Register extends Component {

    constructor(){
        super();
        this.state = {
          valueName: '',
          valueEmail: '',
          valuePassword: '',
          validateName: false,
          validateEmail: false,
          validatePassword: false,
          passwordInvalid: 'min. 8 characters',
          isEmptyName: true,
          isEmptyEmail: true,
          isEmptyPassword: true,

        }
    }

    componentDidUpdate(){
        if (this.props.dataReg.isRegisterSuccess === true){
            this.props.navigation.navigate('SuccessReg')
        }
    }

    handleChangeName = (valueName) => {
        if(valueName===''){
            this.setState({isEmptyName: true})

        } else {
            this.setState({isEmptyName: false})

            if (valueName === ''){
                this.setState({ validateName: false })
            } else {
                this.setState({ validateName: true })
                this.setState({valueName})
                console.log(valueName, "Name is Correct");
            }
        }
    }
    handleChangeEmail = (valueEmail) => {

        if(valueEmail===''){
            this.setState({isEmptyEmail: true})

        } else {
            this.setState({isEmptyEmail: false})

            const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
            if (reg.test(valueEmail) === false) {
                this.setState({ validateEmail: false })
            }
            else {
            this.setState({ validateEmail: true })
            this.setState({ valueEmail })
            }
        }
    }
    handleChangePassword = (valuePassword) => {

        if(valuePassword===''){
            this.setState({isEmptyPassword: true})

        } else {
            this.setState({isEmptyPassword: false})

            const reg1 = /^.{8,}$/;
            const reg2 = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

            if (reg1.test(valuePassword) === false) {
                this.setState({ 
                    validatePassword: false,
                    passwordInvalid: 'min. 8 characters'
                })
            } else if (reg2.test(valuePassword) === false) {
                this.setState({ 
                    validatePassword: false,
                    passwordInvalid: 'min. 1 letter & 1 number'
                })
            } else {
                this.setState({ 
                    valuePassword,
                    validatePassword: true,
                    passwordInvalid: '',
                })
            }
        }
    }

    handleSubmitItem = () => {
        const dataBody = {
            name: this.state.valueName,
            email: this.state.valueEmail,
            password: this.state.valuePassword,
        }
        this.props.submitDataReg(dataBody)
        console.log(dataBody)
    }

    render() {
        // console.log(this.props, 'ini register')
        return (
            <View style={styles.container}>

                <Text style={styles.signupText}>Sign up now!</Text>

                {this.props.dataReg.errorMessage !== null ? 
                    <View style={styles.errorMessageView}>
                        <Text style={styles.errorMessageText}>
                            {this.props.dataReg.errorMessage}
                        </Text> 
                    </View> : null
                }

                <View>
                    <TextInput 
                        onChangeText={this.handleChangeName} 
                        style={styles.formTextInput} 
                        placeholder='Full Name'
                        placeholderTextColor='grey'
                    /> 
                    {
                        this.state.validateName === false && this.state.isEmptyName === false ? 
                        <Text style={styles.formValidationText}>please insert your name</Text> 
                        : null
                    }

                    <TextInput 
                        onChangeText={this.handleChangeEmail} 
                        style={styles.formTextInput} 
                        placeholder='Email'
                        placeholderTextColor='grey'
                    /> 
                    {
                        this.state.validateEmail === false && this.state.isEmptyEmail === false ? 
                        <Text style={styles.formValidationText}>please insert valid email</Text> 
                        : null
                    }

                    <TextInput 
                        onChangeText={this.handleChangePassword} 
                        style={styles.formTextInput} 
                        placeholder='Password'
                        placeholderTextColor='grey'
                        secureTextEntry={true}
                    /> 
                    {
                        this.state.validatePassword === false && this.state.isEmptyPassword === false ? 
                        <Text style={styles.formValidationText}>
                            please insert valid password, {this.state.passwordInvalid}
                        </Text> 
                        : null
                    }

                </View>    

                <TouchableOpacity
                    style={
                        this.state.validateName && 
                        this.state.validateEmail && 
                        this.state.validatePassword ? 
                        styles.registerButton : styles.registerButtonDisabled
                    }
                    onPress={this.handleSubmitItem}
                    disabled={
                        !this.state.validateName && 
                        !this.state.validateEmail && 
                        !this.state.validatePassword
                    }
                >
                    <Text style={styles.registerButtonText}>Register</Text>
                </TouchableOpacity>

                <Text>Already have an account?</Text>

                <TouchableOpacity
                    style={styles.loginButton}
                    onPress={()=>{
                        this.props.navigation.navigate('Login')
                    }}                                                        >
                    <Text style={styles.loginButtonText}>Login</Text>
                </TouchableOpacity>

            </View>
        )
    }
}
