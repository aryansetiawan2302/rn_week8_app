import {
    GET_BOOK_DETAIL_START,
    GET_BOOK_DETAIL_SUCCESS,
    GET_BOOK_DETAIL_FAILED,

    POST_BOOK_FAVORITE_START,
    POST_BOOK_FAVORITE_SUCCESS,
    POST_BOOK_FAVORITE_FAILED,

    GET_FAVORITE_STATUS_START,
    GET_FAVORITE_STATUS_SUCCESS,
    GET_FAVORITE_STATUS_FAILED,

} from 'bookappredux2/src/redux/actions/BookDetailAction'

const initialState = {
    bookDetail: {},
    isLoading: false,
    isFavorite: false,
    postFavorite: false,
    errorMessage: null,
}

export default (state = initialState, action) => {
    // console.log(state, 'ini state bookdetail yang terpanggil')
    // console.log(action.type, 'ini action di bookdetailReducer')

    switch (action.type) {
        case GET_BOOK_DETAIL_START:
            return { ...state, isLoading: true }        
        case GET_BOOK_DETAIL_SUCCESS:
            // console.log('get book detail success')
            return { ...state, bookDetail: action.dataBookDetail, isLoading: false }
        case GET_BOOK_DETAIL_FAILED:
            return { ...state, errorMessage: action.errorMessage, isLoading: false}

        case POST_BOOK_FAVORITE_SUCCESS:
            // console.log('get book detail success')
            return { ...state, postFavorite: true}
        case GET_FAVORITE_STATUS_SUCCESS:
            return { ...state, isFavorite: action.dataIsFavorite}
    
    default:
        return state
    }
}
