import { StyleSheet, Dimensions } from "react-native";
import { WARNA_BG_SCROLL } from 'bookappredux2/src/utils/constant'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({

    background: {
        position:'absolute',
        left:0, 
        right:0, 
        top:0,
        bottom:0
    },
    scrollBackground: {
        backgroundColor: WARNA_BG_SCROLL,
    },
    loading: {
        width: windowWidth,
        height: windowHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },
    greetingText: {
        fontSize: 20,
        padding: 10,
        fontFamily: 'GoudyBookletter1911-Regular'
    },
    sectionText: {
        marginVertical: 10,
        marginLeft: 10,
        fontSize: 22,
        fontFamily: 'LilitaOne-Regular',
    },

    //scroll design
    scrollView: {
        padding: 10,
        // backgroundColor: WARNA_SEKUNDER,
      },
    contentContainer: {
        paddingBottom: 50,
        // backgroundColor: WARNA_SEKUNDER,
    },

    //Recommended Book
    recBookListContainer: {
        flexDirection: 'row',
    },
    recBookContainer: {
        alignItems: 'center',
        // marginBottom: 10,
        marginHorizontal: 5,
        width: 500*0.3,
    },
    recBookImage: {
        width: 500*0.3,
        height: 750*0.3,
        borderRadius: 15,
        marginHorizontal: 5,
    },
    recBookTitle: {
        textAlign: 'center',
        fontSize: 12,
        fontFamily: 'JosefinSans-SemiBold'
    },

    //Popular Book
    popBookListContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginHorizontal: 10,
        flexWrap: 'wrap',
    },
    popBookContainer: {
        alignItems: 'center',
        marginBottom: 10,
        marginHorizontal: 5,
        width: 500*0.2,
    },
    popBookImage: {
        width: 500*0.22,
        height: 750*0.22,
        // marginHorizontal: 5,
        borderRadius: 15,
    },
    popBookTitle: {
        textAlign: 'center',
        fontSize: 10,
        fontFamily: 'JosefinSans-SemiBold',
    },

});
