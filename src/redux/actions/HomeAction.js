import {requestServer} from '../../api/api'

export const GET_BOOK_LIST_START = 'GET_BOOK_LIST_START';
export const GET_BOOK_LIST_SUCCESS = 'GET_BOOK_LIST_SUCCESS';
export const GET_BOOK_LIST_FAILED = 'GET_BOOK_LIST_FAILED';

export const getBookList = () => async dispatch => {
   
    try{
        dispatch({
            type: GET_BOOK_LIST_START,
        })

        const hasilFetchingTotal = await requestServer('get', '/api/v1/books')
        const totalResults = hasilFetchingTotal.data.totalResults

        const hasilFetching = await requestServer('get', `/api/v1/books?limit=${totalResults}`)

        if (hasilFetching.status === 200) {

            dispatch({
                type: GET_BOOK_LIST_SUCCESS,
                dataBookList: hasilFetching.data.results, //masih terbatas 10 buku
            })
        }

    } catch(error) {
        dispatch({
            type: GET_BOOK_LIST_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error')
    }
}
