import { StyleSheet } from "react-native";
import { 
    WARNA_TEXT_BUTTON, 
    WARNA_UTAMA 
} from 'bookappredux2/src/utils/constant'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 40,
    },
    logo: {
        width: 230,
        height: 205,
        marginVertical: 60,
    },
    successText: {
        fontSize: 30,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    directionText: {
        fontSize: 24,
        textAlign: 'center',
    },
    buttonText: {
        fontSize: 15,
        color: WARNA_TEXT_BUTTON
    },
    backButton: {
        marginVertical: 30,
        backgroundColor: WARNA_UTAMA,
        paddingHorizontal: 30,
        paddingVertical: 10,
        borderRadius: 10,
    }
})