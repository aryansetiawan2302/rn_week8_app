import { StyleSheet } from "react-native";
import { 
    WARNA_TEXT_FORM,
    WARNA_BG_FORM,
    WARNA_BG_SCROLL,
} from 'bookappredux2/src/utils/constant'

export const styles = StyleSheet.create({

    background: {
        position:'absolute',
        left:0, 
        right:0, 
        top:0,
        bottom:0
    },
    scrollBackground: {
        backgroundColor: WARNA_BG_SCROLL,
    },
    greetingText: {
        fontSize: 20,
        padding: 10,
        fontFamily: 'GoudyBookletter1911-Regular'
    },

    // Search Section
    searchContainer: {
        margin: 10,
        flexDirection: 'row',
    },
    searchIcon: {
        position: 'absolute',
        padding: 12,
        right: 5,
    },
    formTextInput: {
        flex: 1,
        padding: 5,
        backgroundColor: WARNA_BG_FORM,
        color: WARNA_TEXT_FORM,
        // width: 250,
        borderRadius: 10,
        marginVertical: 5,
    },

    popBookListContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginHorizontal: 10,
        flexWrap: 'wrap',
    },
    popBookContainer: {
        alignItems: 'center',
        marginBottom: 10,
        marginHorizontal: 5,
        width: 500*0.2,
    },
    popBookImage: {
        width: 500*0.22,
        height: 750*0.22,
        borderRadius: 15,
    },
    popBookTitle: {
        textAlign: 'center',
        fontSize: 10,
        fontFamily: 'JosefinSans-SemiBold',
    },
})