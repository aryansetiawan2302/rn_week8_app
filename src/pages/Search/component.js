import React, { Component } from 'react'
import { 
    Text, 
    View, 
    TextInput, 
    TouchableOpacity, 
    Image, 
    ScrollView,
    ImageBackground,
} from 'react-native'
import Icon from 'react-native-vector-icons/dist/Ionicons';
import { SplashBackground } from 'bookappredux2/src/assets';
import {styles} from 'bookappredux2/src/pages/Search/style'

export class Search extends Component {

    constructor(){
        super();
        this.state = {
          valueTitle: '',
          prevTitle: '',
        }
    }

    componentDidUpdate(){
        if(this.state.prevTitle !== this.state.valueTitle){
            this.props.getBookTitleSearch(this.state.valueTitle)
            this.setState({prevTitle: this.state.valueTitle})
        }
    }

    handleChangeTitle = (value) => {
        this.setState({valueTitle: value})
        console.log(value, 'ini value')
    }

    render() {
        return (
            <>
            <ImageBackground source={SplashBackground} style={styles.background}></ImageBackground>            
            <ScrollView style={styles.scrollBackground}>
                <Text style={styles.greetingText}> Hello {this.props.dataLogin.name}!</Text>
                <View style={styles.searchContainer}>
                    <TextInput 
                        onChangeText={this.handleChangeTitle} 
                        style={styles.formTextInput} 
                        placeholder='Search your book'
                        placeholderTextColor='grey'
                    /> 
                    <Icon style={styles.searchIcon} name="search" size={20} color={'lightgray'} />
                </View>

                <View style={styles.popBookListContainer}>
                    {this.props.dataBookTitleSearch.listBookSearch.map(book => (
                        <TouchableOpacity
                            key={book.id}
                            onPress={()=>{
                                this.props.navigation.navigate('BookDetail', {BookId: book.id})
                            }}                                        
                            style={styles.popBookContainer}
                        >
                            <Image 
                                style={styles.popBookImage}
                                source={{
                                    uri: book.cover_image,
                                }}
                            />
                            <Text style={styles.popBookTitle}>{book.title}</Text>

                        </TouchableOpacity>
                    ))}
                </View>

            </ScrollView>
            </>
        )
    }
}
