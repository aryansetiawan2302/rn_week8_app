import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity, Image } from 'react-native'
import { Logo } from 'bookappredux2/src/assets';
import {styles} from 'bookappredux2/src/pages/Login/style'

export class Login extends Component {

    constructor(){
        super();
        this.state = {
          valueEmail: '',
          validateEmail: false,
          valuePassword: '',
          validatePassword: false,
          passwordInvalid: 'min. 8 characters',
          isEmptyEmail: true,
          isEmptyPassword: true,
        }
    }

    componentDidUpdate(){
        if (this.props.dataLogin.isLoginSuccess === true){
            this.props.navigation.navigate('MainApp')
        }
    }

    handleSubmitItem = () => {
        const dataBody = {
            email: this.state.valueEmail,
            password: this.state.valuePassword,
        }
        this.props.submitDataLogin(dataBody)
        console.log(dataBody, 'ini data login')
    }

    handleChangeEmail = (valueEmail) => {
        if(valueEmail===''){
            this.setState({isEmptyEmail: true})

        } else {
            this.setState({isEmptyEmail: false})

            const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
            if (reg.test(valueEmail) === false) {
                this.setState({ validateEmail: false })
            }
            else {
            this.setState({ validateEmail: true })
            this.setState({ valueEmail })
            }
        }
    }

    handleChangePassword = (valuePassword) => {
        if(valuePassword===''){
            this.setState({isEmptyPassword: true})

        } else {
            this.setState({isEmptyPassword: false})

            const reg1 = /^.{8,}$/;
            const reg2 = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

            if (reg1.test(valuePassword) === false) {
                this.setState({ 
                    validatePassword: false,
                    passwordInvalid: 'min. 8 characters'
                })
            } else if (reg2.test(valuePassword) === false) {
                this.setState({ 
                    validatePassword: false,
                    passwordInvalid: 'min. 1 letter & 1 number'
                })
            } else {
                this.setState({ 
                    valuePassword,
                    validatePassword: true,
                    passwordInvalid: '',
                })
                console.log(valuePassword, "Password is Correct");
            }
        }
    }

    render() {
        return (
            <View style={styles.container}>

                <Text style={styles.welcomeText}>Welcome To</Text>

                <Image source={Logo} style={styles.logo}/>                

                {this.props.dataLogin.errorMessage !== null ? 
                    <View style={styles.errorMessageView}>
                        <Text style={styles.errorMessageText}>
                            {this.props.dataLogin.errorMessage}
                        </Text> 
                    </View> : null
                }

                <View>
                    <TextInput 
                        onChangeText={this.handleChangeEmail} 
                        style={styles.formTextInput} 
                        placeholder='Email'
                        placeholderTextColor='grey'
                    /> 
                    {
                        this.state.validateEmail === false && this.state.isEmptyEmail === false ? 
                        <Text style={styles.formValidationText}>please insert valid email</Text> 
                        : null
                    }
                    <TextInput 
                        onChangeText={this.handleChangePassword} 
                        style={styles.formTextInput} 
                        placeholder='Password'
                        placeholderTextColor='grey'
                        secureTextEntry={true}
                    /> 
                    {
                        this.state.validatePassword === false && this.state.isEmptyPassword === false ? 
                        <Text style={styles.formValidationText}>please insert valid password, {this.state.passwordInvalid}</Text> 
                        : null
                    }

                </View>    

                <TouchableOpacity
                    style={this.state.validateEmail && this.state.validatePassword ? styles.loginButton : styles.loginButtonDisabled}
                    onPress={this.handleSubmitItem}
                    disabled={!this.state.validateEmail && !this.state.validatePassword}
                >
                    <Text style={styles.loginButtonText}>Login</Text>
                </TouchableOpacity>

        
                <Text>Dont have an account?</Text>

                <TouchableOpacity
                    style={styles.registerButton}
                    onPress={()=>{
                        this.props.navigation.navigate('Register')
                    }}                                                        >
                    <Text style={styles.registerButtonText}>Register</Text>
                </TouchableOpacity>

            </View>
        )
    }
}