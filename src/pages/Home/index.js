//import Redux
import {connect} from 'react-redux'
//import Action
import {updateStatusLogin} from 'bookappredux2/src/redux/actions/LoginAction'
import {getBookList} from 'bookappredux2/src/redux/actions/HomeAction'
//import Component
import {Home} from 'bookappredux2/src/pages/Home/component'

const mapStateToProps = (state) => ({
    dataLogin: state.LoginReducer,
    dataBook: state.HomeReducer
})
const mapDispatchToProps = {
    updateStatusLogin,
    getBookList, 
}
  
export default connect(mapStateToProps, mapDispatchToProps) (Home)
