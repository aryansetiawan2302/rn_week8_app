//import Redux
import {connect} from 'react-redux'
//import Action
import {getBookTitleSearch} from 'bookappredux2/src/redux/actions/SearchAction'
//import Component
import {Search} from 'bookappredux2/src/pages/Search/component'

const mapStateToProps = (state) => ({
    dataLogin: state.LoginReducer,
    dataBookTitleSearch: state.SearchReducer
})
const mapDispatchToProps = {
    getBookTitleSearch
}
  
export default connect(mapStateToProps, mapDispatchToProps) (Search)
