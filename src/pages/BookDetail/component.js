import React, { Component } from 'react'
import { 
    Text, 
    View, 
    Image, 
    TouchableOpacity, 
    ScrollView,
    ActivityIndicator,
} from 'react-native'
import Icon from 'react-native-vector-icons/dist/Ionicons';
import NumberFormat from 'react-number-format';
import Share from 'react-native-share';
import StarRating from 'react-native-star-rating';
import {notifikasi} from 'bookappredux2/src/components/Notifikasi'
import {styles} from 'bookappredux2/src/pages/BookDetail/style'

export class BookDetail extends Component {

    constructor() {
        super()
    }

    componentDidMount(){
        this.props.getBookDetail(this.props.route.params.BookId)
        this.props.getFavoriteStatus(this.props.route.params.BookId)
    }

    onShare = async () => {
        const shareOptions = {
            title: 'BooKoo',
            message: `Download Bookoo App to get your favorite book now! ${this.props.dataBookDetail.bookDetail.title}`,
            url: this.props.dataBookDetail.bookDetail.cover_image
        }
        try {
            const ShareResponse = await Share.open(shareOptions)
        } catch (error) {
            console.log('Error => ', error)
        }
    }

    handleCreateFavorite = (bookIdValue, bookTitleValue) => {

        const dataBody = {
            bookId: bookIdValue,
        }
        this.props.postBookFavorite(dataBody)

        notifikasi.configure();
        notifikasi.buatChannel('1')
        notifikasi.kirimNotifikasi('1','Congrats!!!', `Success add ${bookTitleValue} to favorite`)

        this.props.getFavoriteStatus(bookIdValue)
    }

    render() {
        // console.log(this.props, 'ini props book detail')

        return (
            <ScrollView>

            {this.props.dataBookDetail.isLoading ? (
                <View style={styles.loading}>
                    <ActivityIndicator size='large' color='red'/>
                </View>
            ): 

                <View>


                <View style={styles.IconContainer}>
                    <TouchableOpacity 
                        style={styles.topIconBack}
                        onPress={()=>{
                            this.props.navigation.goBack(null)
                        }}                                        
                    >
                    <Icon name="arrow-back" size={20} color="darkred"/>
                    </TouchableOpacity>

                    <TouchableOpacity 
                        style={styles.topIconFav}
                        onPress={() => this.handleCreateFavorite(
                            this.props.dataBookDetail.bookDetail.id,
                            this.props.dataBookDetail.bookDetail.title
                        )}
                    >
                        {
                            this.props.dataBookDetail.isFavorite ?
                            <Icon name="heart-sharp" size={20} color="darkred"/> :
                            <Icon name="heart-sharp" size={20} color="white"/>
                        }

                    </TouchableOpacity>

                    <TouchableOpacity 
                        style={styles.topIconShare}
                        onPress={this.onShare}
                    >
                        <Icon name="arrow-redo-outline" size={20} color="darkred"/>
                    </TouchableOpacity>
                </View>

                {console.log(this.props.dataBookDetail.bookDetail.id, 'ini id book detail')}
                <View style={styles.topContainer}>
                    <Image 
                        style={styles.smallPoster}
                        source={{
                            uri: `${this.props.dataBookDetail.bookDetail.cover_image}`,
                        }}
                    />
                    <View style={styles.rightTopContainer}>
                        <View style={styles.ratingView}>
                            <Text style={styles.titleText}>{this.props.dataBookDetail.bookDetail.title}</Text>
                            <Text style={styles.authorText}>{this.props.dataBookDetail.bookDetail.author}</Text>
                            <Text style={styles.authorText}>{this.props.dataBookDetail.bookDetail.publisher}</Text>
                        </View>
                        {/* <Text>Rating: {this.props.dataBookDetail.bookDetail.average_rating}</Text> */}
                        <StarRating
                            disabled={true}
                            rating={Number(this.props.dataBookDetail.bookDetail.average_rating)/2}
                            fullStarColor={'gold'}
                            starSize={30}
                            emptyStarColor={'lightgray'}
                        />
                    </View>
                </View>

                <View style={styles.midContainer}>
                    <View style={styles.midContainerItem}>
                        <Text style={styles.midContainerText}>Stock</Text> 
                        <Text style={styles.midContainerNumber}>{this.props.dataBookDetail.bookDetail.stock_available}</Text>
                    </View>
                    <View style={styles.midContainerItem}>
                        <Text style={styles.midContainerText}>Sold</Text> 
                        <Text style={styles.midContainerNumber}>{this.props.dataBookDetail.bookDetail.total_sale}</Text>
                    </View>
                    <TouchableOpacity style={styles.midContainerButton}>
                        <Icon name="cart" size={20} color="white"/>
                        <Text style={styles.midContainerButtonText}>  Buy   </Text> 
                        <Text style={styles.midContainerButtonText}>
                            <NumberFormat
                                value={this.props.dataBookDetail.bookDetail.price}
                                displayType={'text'}
                                thousandSeparator={true}
                                prefix={'Rp. '}
                                renderText={formattedValue => <Text>{formattedValue}</Text>}
                            />
                        </Text>
                    </TouchableOpacity>

                </View>

                <View style={styles.bottomContainer}>
                    <Text style={styles.overviewText}>Overview</Text>
                    <Text style={styles.contentText}>{this.props.dataBookDetail.bookDetail.synopsis}</Text>
                </View>

                </View>
            }

            </ScrollView>
        )
    }
}
