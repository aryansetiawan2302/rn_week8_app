import React, { Component } from 'react'
import { Text, View, ImageBackground, Image } from 'react-native'
import { SplashBackground, Logo } from 'bookappredux2/src/assets';
import {styles} from 'bookappredux2/src/pages/Splash/style'

export class Splash extends Component {

    componentDidMount() {
        setTimeout(()=>{
            this.props.navigation.replace('Login')
        }, 3000);
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={SplashBackground} style={styles.background}>
                    <Image source={Logo} style={styles.logo}/>
                </ImageBackground>
                <Text style={styles.name}>Aryan</Text>
            </View>
        )
    }
}

export default Splash
