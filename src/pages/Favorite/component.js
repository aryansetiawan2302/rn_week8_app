import React, { Component } from 'react'
import { 
    Text, 
    View, 
    TouchableOpacity, 
    Image, 
    ScrollView, 
    RefreshControl,
    ActivityIndicator,
    ImageBackground
} from 'react-native'
import { SplashBackground } from 'bookappredux2/src/assets'
import {styles} from 'bookappredux2/src/pages/Favorite/style'

export class Favorite extends Component {

    constructor(){
        super()
        this.state = {
            refreshing: false,
            // valueIsFav: '',
            prevIsFav: false,
            prevId: '',
        }
    }

    componentDidMount() {
    //     this.props.getBookFavorite()
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.props.getBookFavorite()
        });
    }
    
    componentWillUnmount() {
        this._unsubscribe();
    }

    onRefresh = async () => {
        this.setState({refreshing: true});
        this.props.getBookFavorite()
        this.setState({refreshing: false});
    }

    render() {
        return (

            <>

            <ImageBackground source={SplashBackground} style={styles.background}></ImageBackground>            
            <ScrollView 
                style={styles.scrollBackground}
                refreshControl={
                    <RefreshControl 
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh}
                    />
                }
            >

            {this.props.dataBookFavorite.isLoading ? (
                <View style={styles.loading}>
                    <ActivityIndicator size='large' color='red'/>
                </View>
            ): 

                <View>
                <Text style={styles.sectionText}> Favorites </Text>

                <View>
                    {this.props.dataBookFavorite.listBookFavorite.map((book, index) => (
                        <>
                        <View style={styles.favBookContainer(index)}>
                            <TouchableOpacity
                                key={book._id}
                                onPress={()=>{
                                    this.props.navigation.navigate('BookDetail', {BookId: book._id})
                                }}                                        
                            >
                                <Image 
                                    style={styles.favBookImage}
                                    source={{
                                        uri: book.cover_image,
                                    }}
                                />
                            </TouchableOpacity>
                            <View style={styles.rightFavContainer}>
                                <Text style={styles.titleText}>{book.title}</Text>
                                <Text style={styles.authorText}>{book.author}</Text>
                                <Text style={styles.authorText}>{book.publisher}</Text>
                            </View>
                        </View>
                        </>

                    ))}
                </View>
                </View>
            }

            </ScrollView>
            </>
        )
    }
}
