import {
    SUBMIT_REG_START,
    SUBMIT_REG_SUCCESS,
    SUBMIT_REG_FAILED,
    UPDATE_STATUS_REG
} from 'bookappredux2/src/redux/actions/RegisterAction'

const initialState = {
    isRegisterSuccess: false,
    isLoading: false,
    errorMessage: null,
}

export default (state = initialState, action) => {
    // console.log(action.type,'ini state register')

    switch (action.type) {
        case SUBMIT_REG_SUCCESS:
            return { ...state, isRegisterSuccess: true, errorMessage: null}
        case SUBMIT_REG_FAILED:
            return { ...state, errorMessage: action.errorMessage}
        case UPDATE_STATUS_REG:
            return { ...state, isRegisterSuccess: false}

        default:
            return state
    }
}
