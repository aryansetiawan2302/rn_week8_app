import {
    GET_BOOK_LIST_START,
    GET_BOOK_LIST_SUCCESS,
    GET_BOOK_LIST_FAILED,
} from 'bookappredux2/src/redux/actions/HomeAction'

const initialState = {
    listBook: [],
    isLoading: false,
    errorMessage: null,
}

export default (state = initialState, action) => {
    // console.log(state, 'ini state home yang terpanggil')
    // console.log(action.dataBookList, 'ini dataBookList di Reducer')

    switch (action.type) {
        case GET_BOOK_LIST_START:
            return { ...state, isLoading: true }
        case GET_BOOK_LIST_SUCCESS:
            return { ...state, listBook: action.dataBookList, isLoading: false }
        case GET_BOOK_LIST_FAILED:
            return { ...state, errorMessage: action.errorMessage, isLoading: false }

    default:
        return state
    }
}
