import Logo from 'bookappredux2/src/assets/images/logo.png'
import SplashBackground from 'bookappredux2/src/assets/images/splashBackground.png'
import SuccessLogo from 'bookappredux2/src/assets/images/successLogo.png'

export {Logo, SplashBackground, SuccessLogo}