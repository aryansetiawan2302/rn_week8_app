import {
    GET_BOOK_FAVORITE_START,
    GET_BOOK_FAVORITE_SUCCESS,
    GET_BOOK_FAVORITE_FAILED,
} from 'bookappredux2/src/redux/actions/FavoriteAction'

const initialState = {
    listBookFavorite: [],
    isLoading: false,
    errorMessage: null,
}

export default (state = initialState, action) => {
    // console.log(state, 'ini state home yang terpanggil')
    // console.log(action.dataBookFavorite, 'ini dataBookFav di Reducer')

    switch (action.type) {
        case GET_BOOK_FAVORITE_START:
            return { ...state, isLoading: true }
        case GET_BOOK_FAVORITE_SUCCESS:
            return { ...state, listBookFavorite: action.dataBookFavorite, isLoading: false }
        case GET_BOOK_FAVORITE_FAILED:
            return { ...state, errorMessage: action.errorMessage}

    default:
        return state
    }
}
